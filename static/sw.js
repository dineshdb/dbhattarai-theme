importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

workbox.routing.registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets',
  }),
);

workbox.routing.registerRoute(
  /^https:\/\/cdnjs\.cloudflare\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'cdnjs',
  }),
);

workbox.routing.registerRoute(
  /^https:\/\/unpkg\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'unpkg',
  }),
);

workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.NetworkFirst(),
);

const articleHandler = workbox.strategies.networkFirst({
  cacheName: 'articles-cache',
  plugins: [
    new workbox.expiration.Plugin({
      maxEntries: 50,
    })
  ]
});

function handleArticles(args) {
  return articleHandler.handle(args).then(response => {
    if (!response) {
      return caches.match('offline.html');
    } else if (response.status === 404) {
      return caches.match('404.html');
    }
    return response;
  })
}

workbox.routing.registerRoute(/(.*)article(.*)\.html/, handleArticles);
workbox.routing.registerRoute(/.*\/$/, handleArticles);
workbox.routing.registerRoute(/api\//, new workbox.strategies.NetworkFirst());

workbox.routing.registerRoute(
  /\.(?:png|jpg|jpeg|svg|gif|ico|webp)$/,
  new workbox.strategies.CacheFirst({
    cacheName: 'image-cache',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 1000,
        maxAgeSeconds: 30 * 24 * 60 * 60,
      })
    ],
  })
);
