export default function swipe(root) {
	let start = null
	let target = null
	root.addEventListener("touchstart", function(event){
		let isSingleTouch = event.touches.length === 1
		start = isSinglTouch ? event.touches[0] : null
		target = isSingleTouch ? event.target : null
	});
	root.addEventListener("touchend", function(event){
		if(start && target){
			let end = event.changedTouches[0];
			let e = new CustomEvent('swipe', { detail: { start, end }, bubbles: true})
			target.dispatchEvent(e)
    	}
  	});
}
